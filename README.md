# Entrepreneurship Resources

Repository of either self-produced pro formas and other contract templates, or other creative commons / open sourced investment documents.

A starting point for would-be entrepreneurs.

The Pro-Formas are all original works by me and MIT licensed for anyone to use.